#pragma once

#include <rsm/kv/store/store.hpp>

#include <rsm/multipaxos/state_machine.hpp>

#include <map>

namespace kv {

using wheels::Result;

using multipaxos::Command;
using multipaxos::CommandOutput;
using multipaxos::IStateMachine;

////////////////////////////////////////////////////////////////////////////////

// State machine implementation for Multi-Paxos

class StateMachine : public IStateMachine {
 public:
  Result<CommandOutput> Apply(const Command& command) override;

 private:
  TinyKVStore kv_;
};

}  // namespace kv
