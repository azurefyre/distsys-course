#pragma once

#include <rsm/multipaxos/rsm.hpp>
#include <rsm/multipaxos/state_machine.hpp>

#include <whirl/node/services.hpp>

#include <string>

namespace multipaxos {

using whirl::NodeServices;

IReplicatedStateMachinePtr MakeMultiPaxosRSM(const std::string& name,
                                             IStateMachinePtr state_machine,
                                             NodeServices runtime);

}  // namespace multipaxos
