#pragma once

#include <rsm/raft/command.hpp>
#include <rsm/raft/rsm_response.hpp>

#include <await/futures/future.hpp>

namespace raft {

using await::futures::Future;

struct IReplicatedStateMachine {
  virtual ~IReplicatedStateMachine() = default;

  virtual void Start() = 0;

  virtual Future<RSMResponse> Execute(Command command) = 0;
};

using IReplicatedStateMachinePtr = std::shared_ptr<IReplicatedStateMachine>;

}  // namespace raft
