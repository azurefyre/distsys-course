#pragma once

#include <rsm/kv/store/store.hpp>

#include <whirl/matrix/history/models/kv.hpp>

namespace whirl::histories {

template <>
std::string KVDefaultValue<std::string>() {
  return "<>";
}

}  // namespace whirl::histories

namespace kv {

using Model = whirl::histories::KVStoreModel<kv::Key, kv::Value>;

}  // namespace kv
